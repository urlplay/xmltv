echo "###### Atualizando o sistema #######"
apt-get update -qq
apt-get install -qq git
apt-get install -y wget
echo "###### Clonando Repositório #######"
git clone -b deploy "http://oauth2:$CI_TOKEN@gitlab.com/urlplay/xmltv.git" output
echo "###### Listar Arquivos #######"
ls -l
echo "##### Baixando Epg ######"
server=$(echo $servervip | base64 --decode)
wget -q -O output/xmltv.xml $server -P output
ls -l
cd output
ls -l
echo "###### Preparar Git #######"
git config --global user.email "$email"
git config --global user.name "$username"
git add *.xml
git commit -m "Update xmltv"
echo "###### Enviar #######"
git push --force origin deploy